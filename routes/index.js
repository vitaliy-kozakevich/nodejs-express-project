var express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
var router = express.Router();
// const userlist = require(./userlist.json);


// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
router.use(bodyParser.json())

/* GET home page. */
router.get('/', (req, res, next) => {
    res.send('Welcome to my first server');
});

// создание функции слушателя для GET-запросов по адресу "/user" — получение массива
router.get('/user', (req, res) => {
    // res.sendFile(__dirname + "/userlist.json");
    fs.readFile('./userlist.json', 'utf8', (err, data) => {
      if(err) throw err;
      console.log('Get arr of users');
      res.send(JSON.parse(data));
    });
  })

// создание функции слушателя для GET-запросов по адресу "/user/:id" — получение юзера
router.get('/user/:id', (req, res) => {
  // res.send(userlist[req.params["id"]]);
  fs.readFile('./userlist.json', 'utf8', (err, data) => {
    if(err) throw err;
    parseList = JSON.parse(data)
    console.log(parseList[req.params["id"]]);
    res.send(parseList[req.params["id"]]);
  });
})

// создание функции слушателя для PUT-запросов по адресу "/user/:id"
router.put('/user/:id', (req, res) => {
  // userlist[req.params["id"]] = req.body;
  // console.log(userlist);
  // res.send(`Update ${userlist[req.params["id"]].name}`)
  fs.readFile('./userlist.json', 'utf8', (err, data) => {
    if (err){
        console.log(err);
    } else {
    obj = JSON.parse(data); 
    console.log(obj[req.params["id"]] = req.body);
    obj[req.params["id"]]._id = req.params["id"];
    json = JSON.stringify(obj);
    fs.writeFile('./userlist.json', json, 'utf8', (err, data) => {
      if(err) throw err;
      res.send('User have been updated!!!');
    });
  }})
})

// создание функции слушателя для POST-запросов по адресу "/user"
router.post('/user', (req, res) => {
    // console.log(req.body);
    // userlist.push(req.body);
    // res.send(`Hello ${userlist[17].name}`)
  fs.readFile('./userlist.json', 'utf8', (err, data) => {
    if (err){
        console.log(err);
    } else {
    obj = JSON.parse(data); 
    obj.push(req.body); 
    json = JSON.stringify(obj);
    fs.writeFile('./userlist.json', json, 'utf8', (err, data) => {
      if(err) throw err;
      res.send('User have been added!!!');
    });
  }}) 
})

// создание функции слушателя для DELETE-запросов по адресу "/user/:id"
router.delete('/user/:id', (req, res) => {
  // const deleteFighter = req.params["id"];
  // userlist.splice(deleteFighter, 1);
  // res.send(`Delete ${userlist[deleteFighter].name}`)
  deleteFighter = req.params["id"];
    fs.readFile('./userlist.json', 'utf8', (err, data) => {
    if (err){
        console.log(err);
    } else {
    obj = JSON.parse(data); 
    console.log(obj.splice(deleteFighter, 1));
    json = JSON.stringify(obj);
    fs.writeFile('./userlist.json', json, 'utf8', (err, data) => {
      if(err) throw err;
      res.send('User have been deleted!!!');
    });
  }})
})

module.exports = router;
