var express = require('express');
const bodyParser = require('body-parser');
var router = express.Router();

const { saveName } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
router.use(bodyParser.json())

// router.get('/', function(req, res, next) {
//   res.send(req.body);
// });
// router.post('/', isAuthorized, function(req, res, next) {
//   const result = saveName(req.body);

//   if (result) {
//     res.send(`Your name is ${result}`);
//   } else {
//     res.status(400).send(`Some error`);
//   }
// });

module.exports = router;